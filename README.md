# The project was terminated on 06.11.2024.

# DO NOT REPORT ANY ISSUES!

# FreeBSD Kernel Module Interface Rust Crate ![KPI logo](https://gitlab.com/4neko/freebsd-kmod-kpi/-/raw/master/logo_600.png?ref_type=heads&inline=true)

**This repository contains everything which is needed to build a kernel module and safe realizations! A bindings to FreeBSD Kernel is provided by [FreeBSD-KPI-RS](https://gitlab.com/relkom/freebsd-kpi-rs) crate.**

## Supportes FreeBSD versions:
* 13.1 RELENG (RELEASE) !!! no longer maintained.
* 14.0 RELENG (RELEASE) <-- current

## Releases
!!! Only latest (major) versions of FreeBSD will be actively supported. As soon as new major version is released, the previous are automatically moved to `legacy`. 

For example: current 13.1. When 14.0 will be released, the current will become 14.0

At the moment it is not possible to have available an `KPI` or `KMI` for x.x-CURRENT or x.x-STABLE.

## Feature
### MALLOC
`GLOBAL_ALLOC_DISABLED` - disables the build-in implementation of the kernel allocator.

Either:
`GLOBAL_ALLOC_FLAG_WAITOK` - tells the malloc that your kernel module is ok to wait for heap space.
Or:
`GLOBAL_ALLOC_FLAG_NOWAIT` - tells the malloc that your kernel module should not wait for heap space.
`GLOBAL_ALLOC_FLAG_USERESERVE` - additional feature which alters the behaviour of NOWAIT.

One of the above should be declared.


`GLOBAL_ALLOC_EXCLUDE_PANICHANDLER` - excludes the default `\#\[panic_handler\]`
`GLOBAL_ALLOC_EXCLUDE_ALLOC_ERROR_HNDL`- excludes the `\#\[alloc_error_handler\]`

## Programming guide

### HEAP

By default, this crate implements the common `GlobalAllocator``. It is recomended not to use it and call a macro from `global_alloc.rs` to implement your own. Also, this crate should be compiled setting feature `GLOBAL_ALLOC_DISABLED`.

Unfortunatelly, a GlobalAllocator is required by default.

A default `panic_handler` and `alloc_error_handler` are included as part of the GlobalAllocator. Bothe can be overrided by including features: `GLOBAL_ALLOC_EXCLUDE_PANICHANDLER` `GLOBAL_ALLOC_EXCLUDE_ALLOC_ERROR_HNDL`.

A custom instances of `malloc` can be declared using macro in the `heap_alloc/alloc.rs`.

## Requests

todo
